import sys
import numpy
import matplotlib.pyplot as plotter
from sklearn.ensemble import RandomForestClassifier


MODEL_SETTINGS = {'number_of_estimators':20, 'dataset_settings':{'train_number':40000}}

# Initializing dependent constants
TRAIN_NUMBER = MODEL_SETTINGS['dataset_settings']['train_number']

def load_train_data():
    list_of_instances = []
    list_of_labels = []
    with open('./data/train.csv') as input_stream:
        input_stream.readline()
        for line in input_stream:
            data_array = list(map(int, line[:-1].split(',')))
            # print(data_array)
            # print(len(data_array))
            label = data_array[0]
            np_image = numpy.array(data_array[1:]).reshape((28, 28))
            # plotter.imshow(np_image)
            # plotter.show()
            # input()
            list_of_instances.append(np_image)
            list_of_labels.append(label)
        # sys.exit()
    return list_of_instances, list_of_labels

if __name__ == '__main__':
    list_of_instances, list_of_labels = load_train_data()
    # for instance, label in zip(list_of_instances, train_labels):
    #     print(label)
    #     plotter.imshow(instance)
    #     plotter.show()
    print(len(list_of_instances), len(list_of_labels))
    train_instances = list_of_instances[:TRAIN_NUMBER]
    train_labels = list_of_labels[:TRAIN_NUMBER]
    valid_instances = list_of_instances[TRAIN_NUMBER:]
    valid_labels = list_of_labels[TRAIN_NUMBER:]

    train_samples = [instance.reshape(-1) for instance in train_instances]
    valid_samples = [instance.reshape(-1) for instance in valid_instances]

    model = RandomForestClassifier(n_estimators=MODEL_SETTINGS['number_of_estimators'])
    model.fit(train_samples, train_labels)

    right_predictions = 0
    for sample, label in zip(valid_samples, valid_labels):
        prediction = model.predict(sample.reshape(1, -1))[0]
        # print(prediction, label)
        # input()
        if prediction == label:
            right_predictions += 1

    print(right_predictions)
